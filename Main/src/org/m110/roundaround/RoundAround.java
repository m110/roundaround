package org.m110.roundaround;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import org.m110.roundaround.screens.GameScreen;
import org.m110.roundaround.screens.LevelScreen;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class RoundAround extends Game {

    private InputMultiplexer inputMultiplexer;

    @Override
    public void create() {

        inputMultiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(inputMultiplexer);

        setGameScreen(new LevelScreen(this));
    }

    protected void setGameScreen(GameScreen screen) {
        // Remove input multiplexer of current screen
        if (getScreen() != screen && getScreen() instanceof GameScreen) {
            GameScreen gameScreen = (GameScreen) getScreen();
            inputMultiplexer.removeProcessor(gameScreen.getInputMultiplexer());
        }

        setScreen(screen);
        inputMultiplexer.addProcessor(screen.getInputMultiplexer());
    }
}
