package org.m110.roundaround.screens;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import org.m110.roundaround.RoundAround;
import org.m110.roundaround.actors.balls.Ball;
import org.m110.roundaround.actors.Player;
import org.m110.roundaround.actors.balls.ColorBall;
import org.m110.roundaround.actors.balls.SquareBall;
import org.m110.roundaround.core.Timer;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class LevelScreen extends GameScreen {

    protected final Player player;
    protected final Array<Ball> balls;

    protected final Timer spawnTimer;

    public LevelScreen(RoundAround client) {
        super(client);

        player = new Player();
        balls = new Array<>();
        spawnTimer = new Timer(0.5f);

        stage.addActor(player);
        stage.addListener(player.getPlayerInput());

        inputMultiplexer.addProcessor(new GestureDetector(player.getPlayerInput()));
        inputMultiplexer.addProcessor(stage);

        player.initArmSet();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if (spawnTimer.ready()) {
            Ball ball = null;
            switch (MathUtils.random(1)) {
                case 0: ball = new ColorBall(player); break;
                case 1: ball = new SquareBall(player); break;
            }

            stage.addActor(ball);
            balls.add(ball);
            spawnTimer.reset();
        } else spawnTimer.update(delta);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
