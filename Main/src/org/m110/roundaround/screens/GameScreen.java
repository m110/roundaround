package org.m110.roundaround.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.Stage;
import org.m110.roundaround.RoundAround;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public abstract class GameScreen implements Screen {
    protected final RoundAround client;
    protected final InputMultiplexer inputMultiplexer;

    protected final Stage stage;
    protected final OrthographicCamera camera;

    public GameScreen(RoundAround client) {
        this.client = client;

        inputMultiplexer = new InputMultiplexer();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.update();

        stage = new Stage();
        stage.setCamera(camera);
    }

    public InputMultiplexer getInputMultiplexer() {
        return inputMultiplexer;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(width, height, true);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
