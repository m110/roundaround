package org.m110.roundaround.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Entity extends Actor {

    private TextureRegion texture;

    public Entity() {

    }

    public Entity(String textureName) {
        texture = new TextureRegion(new Texture(Gdx.files.internal(textureName + ".png")));
    }

    @Override
    public void draw(SpriteBatch batch, float parentAlpha) {
        if (texture != null) {
            batch.draw(texture, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(),
                       getScaleX(), getScaleY(), getRotation());
        }
    }

    public void setWorldPosition(float x, float y) {
        setWorldX(x);
        setWorldY(y);
    }

    public void setWorldX(float x) {
        setX(x - getOriginX());
    }

    public void setWorldY(float y) {
        setY(y - getOriginY());
    }

    public float getWorldX() {
        return getX() + getOriginX();
    }

    public float getWorldY() {
        return getY() + getOriginY();
    }

    public float getAngleTo(Entity entity) {
        double a1 = Math.atan2(getWorldY() - entity.getWorldY(), getWorldX() - entity.getWorldX());
        double a2 = Math.atan2(0.0, getWorldX() - entity.getWorldX());

        float degrees = 360.0f + (float) Math.toDegrees(a1 - a2);
        if (entity.getWorldX() <= getWorldX()) {
            degrees -= 180.0f;
        }

        return degrees;
    }
}
