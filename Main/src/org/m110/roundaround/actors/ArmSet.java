package org.m110.roundaround.actors;

import com.badlogic.gdx.utils.Array;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class ArmSet {

    private final Player player;

    private final Array<Arm> arms;

    public ArmSet(Player player) {
        this.player = player;
        arms = new Array<>();
    }

    public void addArm(float dx, float dy, float rotation) {
        Arm arm = new Arm(player, dx, dy, rotation);
        player.getStage().addActor(arm);
        arms.add(arm);
    }
}
