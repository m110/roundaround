package org.m110.roundaround.actors.balls;

import org.m110.roundaround.actors.Player;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class ColorBall extends Ball {
    public ColorBall(Player player) {
        super(player);
    }

    @Override
    public void action() {
        player.addPoints(1);
    }
}
