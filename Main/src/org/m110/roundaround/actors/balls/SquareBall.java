package org.m110.roundaround.actors.balls;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import org.m110.roundaround.actors.Player;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class SquareBall extends Ball {
    public SquareBall(Player player) {
        super(player);
        color = Color.DARK_GRAY;
    }

    @Override
    public void action() {
        player.takeHealthPoints(1);
    }

    @Override
    public void draw(SpriteBatch batch, float parentAlpha) {
        batch.end();
        renderer.setProjectionMatrix(batch.getProjectionMatrix());
        renderer.setColor(color);
        renderer.begin(ShapeRenderer.ShapeType.FilledRectangle);
        renderer.filledRect(getX(), getY(), getWidth(), getHeight());
        renderer.end();
        batch.begin();
    }
}
