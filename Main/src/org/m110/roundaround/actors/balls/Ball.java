package org.m110.roundaround.actors.balls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import org.m110.roundaround.actors.Entity;
import org.m110.roundaround.actors.Player;
import org.m110.roundaround.core.Timer;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public abstract class Ball extends Entity {

    protected final Player player;
    protected final ShapeRenderer renderer;

    protected final float radius;
    protected final float velocity;
    protected Color color;

    protected final Timer timer;

    public Ball(Player player) {
        this.player = player;
        renderer = new ShapeRenderer();
        timer = new Timer(0.02f);

        radius = MathUtils.random(5.0f, 10.0f);
        velocity = MathUtils.random(2.0f, 8.0f);
        color = new Color(MathUtils.random(0.3f, 1.0f), MathUtils.random(0.3f, 1.0f), MathUtils.random(0.3f, 1.0f), 1.0f);

        if (MathUtils.random(1) == 0) {
            setPosition(0.0f, MathUtils.random(Gdx.graphics.getHeight()));
        } else {
            setPosition(MathUtils.random(Gdx.graphics.getWidth()), 0.0f);
        }

        setSize(radius, radius);
        setOrigin(radius / 2.0f, radius / 2.0f);

        setRotation(getAngleTo(player));
    }

    public abstract void action();

    @Override
    public void draw(SpriteBatch batch, float parentAlpha) {
        batch.end();
        renderer.setProjectionMatrix(batch.getProjectionMatrix());
        renderer.setColor(color);
        renderer.begin(ShapeRenderer.ShapeType.FilledCircle);
        renderer.filledCircle(getWorldX(),getWorldY(), radius);
        renderer.end();
        batch.begin();
    }

    @Override
    public void act(float delta) {
        if (timer.ready()) {
            setX(getX() + (float) Math.cos(Math.toRadians((double) getRotation())) * velocity);
            setY(getY() + (float) Math.sin(Math.toRadians((double) getRotation())) * velocity);
            timer.reset();
        } else timer.update(delta);
    }
}
