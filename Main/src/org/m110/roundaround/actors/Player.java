package org.m110.roundaround.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import org.m110.roundaround.input.PlayerInput;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Player extends Entity{

    private final PlayerInput playerInput;

    private final ArmSet armSet;

    private int healthPoints = 5;
    private int points = 0;

    private static final float MAX_VELOCITY = 500.0f;
    private float velocity = 0.0f;

    public Player() {
        super("player");

        armSet = new ArmSet(this);
        playerInput = new PlayerInput(this);

        setSize(Gdx.graphics.getWidth() / 13.0f, Gdx.graphics.getWidth() / 13.0f);
        setOrigin(getWidth() / 2.0f, getHeight() / 2.0f);
        setPosition(Gdx.graphics.getWidth() / 2.0f - getOriginX(), Gdx.graphics.getHeight() / 2.0f - getOriginY());
    }

    public void initArmSet() {
        float distance = getWidth();
        armSet.addArm(distance, 0.0f, 0.0f);
        armSet.addArm(0.0f, distance, 90.0f);
        armSet.addArm(-distance, 0.0f, 180.0f);
        armSet.addArm(0.0f, -distance, 270.0f);
    }

    @Override
    public void act(float delta) {
        if (velocity < 0.0f) {
            velocity += delta * 100;
        } else if (velocity > 0.0f) {
            velocity -= delta * 100;
        }

        setRotation(getRotation() + velocity / 100.0f);
    }

    public void addPoints(int points) {
        this.points += points;
    }

    public int getPoints() {
        return points;
    }

    public void addHealthPoints(int healthPoints) {
        this.healthPoints += healthPoints;
    }

    public void takeHealthPoints(int healthPoints) {
        if (this.healthPoints - healthPoints >= 0) {
            this.healthPoints -= healthPoints;
        } else {
            this.healthPoints = 0;
        }
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

    public void addVelocity(float velocity) {
        if (Math.signum(this.velocity) != Math.signum(velocity)) {
            this.velocity = 0;
        }

        this.velocity += velocity;

        if (Math.abs(this.velocity) > MAX_VELOCITY) {
            this.velocity = Math.signum(this.velocity) * MAX_VELOCITY;
        }
    }

    public PlayerInput getPlayerInput() {
        return playerInput;
    }
}
