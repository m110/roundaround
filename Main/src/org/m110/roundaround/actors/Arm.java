package org.m110.roundaround.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Arm extends Entity {

    private final Player player;
    private final float rotation;
    private final float radius;

    public Arm(Player player, float dx, float dy, float rotation) {
        super("arm");

        this.player = player;
        this.rotation = rotation;

        setWidth(Gdx.graphics.getWidth() / 2.0f - 3 * player.getWidth());
        setHeight(player.getHeight() / 2.0f);
        setOrigin(getWidth() / 2.0f,  getHeight() / 2.0f);
        setRotation(rotation);

        if (dx > 0.0f) dx += getOriginX();
        else if (dx < 0.0f) dx -= getOriginX();

        if (dy > 0.0f) dy += getOriginX();
        else if (dy < 0.0f) dy -= getOriginX();

        setWorldPosition(player.getWorldX() + dx, player.getWorldY() + dy);

        radius = (float) Math.sqrt(Math.pow(player.getWorldX() - getWorldX(), 2) +
                                   Math.pow(player.getWorldY() - getWorldY(), 2));
    }

    @Override
    public void act(float delta) {
        //setWorldPosition(player.getWorldX() + dx, player.getWorldY() + dy);

        setRotation(player.getRotation() + rotation);
        setWorldPosition(player.getWorldX() + (float) Math.cos(Math.toRadians((double) getRotation() + 180.0f)) * radius,
                         player.getWorldY() + (float) Math.sin(Math.toRadians((double) getRotation() + 180.0f)) * radius);
    }
}
