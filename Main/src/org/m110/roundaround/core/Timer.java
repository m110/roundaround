package org.m110.roundaround.core;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Timer {

    protected float time;
    protected final float interval;

    public Timer(float interval) {
        this.interval = interval;
        time = interval;
    }

    public void update(float delta) {
        if (time > 0.0f) {
            time -= delta;
        }
    }

    public void reset() {
        time = interval;
    }

    public boolean ready() {
        return time <= 0.0f;
    }

    public void disable() {
        time = 0.0f;
    }

    public float getPercentDone() {
        return time / interval;
    }
}
