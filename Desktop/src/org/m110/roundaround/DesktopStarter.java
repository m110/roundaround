package org.m110.roundaround;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class DesktopStarter {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "HappyBalls";
        cfg.useGL20 = true;
        cfg.width = 640;
        cfg.height = 480;
        new LwjglApplication(new RoundAround(), cfg);
    }
}
